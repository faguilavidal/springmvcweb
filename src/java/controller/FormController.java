package controller;

import java.util.LinkedHashMap;
import java.util.Map;
import model.Persona;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("form.htm")
public class FormController {
    
    @RequestMapping(method=RequestMethod.GET)
    public ModelAndView form()
    {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("form/form");
        mav.addObject("persona", new Persona());
        return mav;
    }
    
    //Metodo para poblar nuestro select
    @ModelAttribute("paisLista")
    public Map<String,String> listadoPaises()
    {
        Map<String,String> pais = new LinkedHashMap<>();
        pais.put("1", "Chile");
        pais.put("2", "Argentina");
        pais.put("3", "Bolivia");
        pais.put("4", "Peru");
        pais.put("5", "Colombia");
        pais.put("6", "Venezuela");
        pais.put("7", "Uruguay");
        return pais;
    }
    
    
    
    /*
    @RequestMapping(method=RequestMethod.POST)
    public String form(Persona per, ModelMap model)
    {
        model.addAttribute("nombre",per.getNombre());
        model.addAttribute("correo",per.getCorreo());
        model.addAttribute("pais",per.getPais());
        return "exito/exito";
    }
    */
}
