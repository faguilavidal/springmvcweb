<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="<c:url value="/node_modules/bootstrap/dist/css/bootstrap.min.css" />
        
    </head>
    <body>
        <div class="container">
        	<div class="row">
                <div class="col">
                    <h1>Ingrese sus Datos</h1>
                    <form:form method="post" commandName="persona">
                        <p>
                            <form:label path="nombre">Nombre</form:label>
                            <form:input path="nombre" cssClass="form-control"/>
                        </p>
                        <p>
                            <form:label path="correo">Correo</form:label>
                            <form:input path="correo" cssClass="form-control"/>
                        </p>
                        <p>
                            <form:label path="pais">Pais</form:label>
                            <form:select path="pais" cssClass="form-control">
                                <form:option value="0">Seleccione...</form:option>
                                <form:options items="${paisLista}" />
                            </form:select>
                        </p>
                        <hr>
                        <input type="submit" value="Enviar" class="form-control">
                    </form:form>
                </div>
        	</div>
        </div>
    </body>
</html>
