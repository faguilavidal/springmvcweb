<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Ejemplo - Otro</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="content">
            <h1>#manosenelcodigo</h1>
            <table class="table table-borderer m-5">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Fabian Aguila</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Luis Pai</td>
                </tr>
            </tbody>
        </table>
            <hr/>
            <div class="text-center">
                <img class="rounded" src="<c:url value="/public/images/internet128.png" />" />
            </div>
            <div class="card m-3">
                <div class="card-body">
                    Basic Card Example
                </div>
            </div>
            <hr/>
            <a href="<c:url value="/home.htm" />">Ir a Home</a>
        </div>        
    </body>
</html>
